#install.packages("rJava")
library (foreign)
library (xlsx)
library (scales)
library (Metrics)
rm(list = ls())
#### Export the volume-assignd highway network as a dbf file to folder L:\travel_demand_modeling\TDM2045\Validation======
# Read in files
TDMVol<-read.dbf("L:/Freight Model/CUUATS Cargo Input/20 Validation/PassTruckTotalAss.dbf")
VolumeValidation_CountyTotalVolume<-read.xlsx("L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidation.xlsx",sheetName="Champaign County Total Volume")
VolumeValidation_MPATotalVolume<-read.xlsx("L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidation.xlsx",sheetName="MPA Total Volume")
VolumeValidation_ValidationLinkTotalVolume<-read.xlsx("L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidation.xlsx",sheetName="Validation Links Total Volume")
VolumeValidation_CountyTruckVolume<-read.xlsx("L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidation.xlsx",sheetName="Champaign County Truck Volume")
VolumeValidation_MPATruckVolume<-read.xlsx("L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidation.xlsx",sheetName="MPA Truck Volume")
VolumeValidation_ValidationLinkTruckVolume<-read.xlsx("L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidation.xlsx",sheetName="Validation Links Truck Volume")
ScreenlineValidation<-read.xlsx("L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidation.xlsx",sheetName="Screenline Total Volume")
CountyLinks=TDMVol[which(TDMVol$AADT!=0),]
MPALinks<-read.csv("L:/travel_demand_modeling/TDM2045/Validation/MPALinks.csv")
MPALinksJoin<-merge(x = MPALinks, y = TDMVol, by = "LINK_ID", all.x = TRUE)
MPALinksJoin=MPALinksJoin[which(MPALinksJoin$AADT!=0),]
remove(MPALinks)
ValidationLinks<-read.csv("L:/travel_demand_modeling/TDM2045/Validation/ValidationLink.csv")
ValidationLinksJoin<-merge(x = ValidationLinks, y = TDMVol, by = "LINK_ID", all.x = TRUE)
ValidationLinksJoin=ValidationLinksJoin[which(ValidationLinksJoin$AADT!=0),]
remove(ValidationLinks)
ScreenlineLinks<-read.csv("L:/travel_demand_modeling/TDM2045/Validation/ScreenlineLinkID.csv")
ScreenlineLinksJoin<-merge(x = ScreenlineLinks, y = TDMVol, by = "LINK_ID", all.x = TRUE)
ScreenlineLinksJoin=ScreenlineLinksJoin[which(ScreenlineLinksJoin$AADT!=0),]
remove(ScreenlineLinks)
ValidationLinks_Interstates=ValidationLinksJoin[which(ValidationLinksJoin$FACILITY>=5&ValidationLinksJoin$FACILITY<=7),]
ValidationLinks_MajorArterial=ValidationLinksJoin[which(ValidationLinksJoin$FACILITY==4),]
ValidationLinks_MinorArterial=ValidationLinksJoin[which(ValidationLinksJoin$FACILITY==3),]
ValidationLinks_Collector=ValidationLinksJoin[which(ValidationLinksJoin$FACILITY==2),]
County_Interstates=CountyLinks[which(CountyLinks$FACILITY==7),]
County_MajorArterial=CountyLinks[which(CountyLinks$FACILITY==4),]
County_MinorArterial=CountyLinks[which(CountyLinks$FACILITY==3),]
County_Collector=CountyLinks[which(CountyLinks$FACILITY==2),]
MPA_Interstates=MPALinksJoin[which(MPALinksJoin$FACILITY==7),]
MPA_MajorArterial=MPALinksJoin[which(MPALinksJoin$FACILITY==4),]
MPA_MinorArterial=MPALinksJoin[which(MPALinksJoin$FACILITY==3),]
MPA_Collector=MPALinksJoin[which(MPALinksJoin$FACILITY==2),]
SL_1=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==1),]
SL_2=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==2),]
SL_3=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==3),]
SL_4=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==4),]
SL_5=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==5),]
SL_6=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==6),]
SL_7=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==7),]
SL_8=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==8),]
SL_9=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==9),]
SL_10=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==10),]
SL_11=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==11),]
SL_12=ScreenlineLinksJoin[which(ScreenlineLinksJoin$Screenline==12),]
# Validation Links=====
##For interstates, recalcualte V1T as the sum of two links with the same AADT and HCV
for (i in (1:nrow(ValidationLinks_Interstates))){
  for (j in (1:nrow(ValidationLinks_Interstates))){
    if ((ValidationLinks_Interstates[i,"AADT"]==ValidationLinks_Interstates[j,"AADT"])&(ValidationLinks_Interstates[i,"HCV"]==ValidationLinks_Interstates[j,"HCV"])&(i!=j)){
      ValidationLinks_Interstates[i,"V1T_1"]=ValidationLinks_Interstates[i,"V1_1"]+ValidationLinks_Interstates[j,"V1_1"]
      j=j+1}
  }
  i=i+1
}
VolumeValidation_ValidationLinkTotalVolume[1,2]=nrow(ValidationLinks_Interstates)
VolumeValidation_ValidationLinkTotalVolume[1,3]=percent((sum(ValidationLinks_Interstates$V1T_1)-sum(ValidationLinks_Interstates$AADT))/sum(ValidationLinks_Interstates$AADT))
VolumeValidation_ValidationLinkTotalVolume[1,4]=percent((sum(ValidationLinks_Interstates$V1T_1*ValidationLinks_Interstates$DISTANCE)-sum(ValidationLinks_Interstates$AADT*ValidationLinks_Interstates$DISTANCE))/sum(ValidationLinks_Interstates$AADT*ValidationLinks_Interstates$DISTANCE))
VolumeValidation_ValidationLinkTotalVolume[1,6]=rmse(ValidationLinks_Interstates$AADT,ValidationLinks_Interstates$V1T_1)
VolumeValidation_ValidationLinkTotalVolume[1,7]=percent(VolumeValidation_ValidationLinkTotalVolume[1,6]/(sum(ValidationLinks_Interstates$AADT)/nrow(ValidationLinks_Interstates)))
VolumeValidation_ValidationLinkTotalVolume[1,8]=cor(ValidationLinks_Interstates$AADT,ValidationLinks_Interstates$V1T_1)^2
VolumeValidation_ValidationLinkTotalVolume[2,2]=nrow(ValidationLinks_MajorArterial)
VolumeValidation_ValidationLinkTotalVolume[2,3]=percent((sum(ValidationLinks_MajorArterial$V1T_1)-sum(ValidationLinks_MajorArterial$AADT))/sum(ValidationLinks_MajorArterial$AADT))
VolumeValidation_ValidationLinkTotalVolume[2,4]=percent((sum(ValidationLinks_MajorArterial$V1T_1*ValidationLinks_MajorArterial$DISTANCE)-sum(ValidationLinks_MajorArterial$AADT*ValidationLinks_MajorArterial$DISTANCE))/sum(ValidationLinks_MajorArterial$AADT*ValidationLinks_MajorArterial$DISTANCE))
VolumeValidation_ValidationLinkTotalVolume[2,6]=rmse(ValidationLinks_MajorArterial$AADT,ValidationLinks_MajorArterial$V1T_1)
VolumeValidation_ValidationLinkTotalVolume[2,7]=percent(VolumeValidation_ValidationLinkTotalVolume[2,6]/(sum(ValidationLinks_MajorArterial$AADT)/nrow(ValidationLinks_MajorArterial)))
VolumeValidation_ValidationLinkTotalVolume[2,8]=cor(ValidationLinks_MajorArterial$AADT,ValidationLinks_MajorArterial$V1T_1)^2
VolumeValidation_ValidationLinkTotalVolume[3,2]=nrow(ValidationLinks_MinorArterial)
VolumeValidation_ValidationLinkTotalVolume[3,3]=percent((sum(ValidationLinks_MinorArterial$V1T_1)-sum(ValidationLinks_MinorArterial$AADT))/sum(ValidationLinks_MinorArterial$AADT))
VolumeValidation_ValidationLinkTotalVolume[3,4]=percent((sum(ValidationLinks_MinorArterial$V1T_1*ValidationLinks_MinorArterial$DISTANCE)-sum(ValidationLinks_MinorArterial$AADT*ValidationLinks_MinorArterial$DISTANCE))/sum(ValidationLinks_MinorArterial$AADT*ValidationLinks_MinorArterial$DISTANCE))
VolumeValidation_ValidationLinkTotalVolume[3,6]=rmse(ValidationLinks_MinorArterial$AADT,ValidationLinks_MinorArterial$V1T_1)
VolumeValidation_ValidationLinkTotalVolume[3,7]=percent(VolumeValidation_ValidationLinkTotalVolume[3,6]/(sum(ValidationLinks_MinorArterial$AADT)/nrow(ValidationLinks_MinorArterial)))
VolumeValidation_ValidationLinkTotalVolume[3,8]=cor(ValidationLinks_MinorArterial$AADT,ValidationLinks_MinorArterial$V1T_1)^2
VolumeValidation_ValidationLinkTotalVolume[4,2]=nrow(ValidationLinks_Collector)
VolumeValidation_ValidationLinkTotalVolume[4,3]=percent((sum(ValidationLinks_Collector$V1T_1)-sum(ValidationLinks_Collector$AADT))/sum(ValidationLinks_Collector$AADT))
VolumeValidation_ValidationLinkTotalVolume[4,4]=percent((sum(ValidationLinks_Collector$V1T_1*ValidationLinks_Collector$DISTANCE)-sum(ValidationLinks_Collector$AADT*ValidationLinks_Collector$DISTANCE))/sum(ValidationLinks_Collector$AADT*ValidationLinks_Collector$DISTANCE))
VolumeValidation_ValidationLinkTotalVolume[4,6]=rmse(ValidationLinks_Collector$AADT,ValidationLinks_Collector$V1T_1)
VolumeValidation_ValidationLinkTotalVolume[4,7]=percent(VolumeValidation_ValidationLinkTotalVolume[4,6]/(sum(ValidationLinks_Collector$AADT)/nrow(ValidationLinks_Collector)))
VolumeValidation_ValidationLinkTotalVolume[4,8]=cor(ValidationLinks_Collector$AADT,ValidationLinks_Collector$V1T_1)^2
# County=====
VolumeValidation_CountyTotalVolume[1,2]=nrow(County_Interstates)
VolumeValidation_CountyTotalVolume[1,3]=percent((sum(County_Interstates$V1T_1)*2-sum(County_Interstates$AADT))/sum(County_Interstates$AADT))
VolumeValidation_CountyTotalVolume[1,4]=percent((sum(County_Interstates$V1T_1*County_Interstates$DISTANCE*2)-sum(County_Interstates$AADT*County_Interstates$DISTANCE))/sum(County_Interstates$AADT*County_Interstates$DISTANCE))
VolumeValidation_CountyTotalVolume[1,6]=rmse(County_Interstates$AADT,County_Interstates$V1T_1*2)
VolumeValidation_CountyTotalVolume[1,7]=percent(VolumeValidation_CountyTotalVolume[1,6]/(sum(County_Interstates$AADT)/nrow(County_Interstates)))
VolumeValidation_CountyTotalVolume[1,8]=cor(County_Interstates$AADT,County_Interstates$V1T_1*2)^2
VolumeValidation_CountyTotalVolume[2,2]=nrow(County_MajorArterial)
VolumeValidation_CountyTotalVolume[2,3]=percent((sum(County_MajorArterial$V1T_1)-sum(County_MajorArterial$AADT))/sum(County_MajorArterial$AADT))
VolumeValidation_CountyTotalVolume[2,4]=percent((sum(County_MajorArterial$V1T_1*County_MajorArterial$DISTANCE)-sum(County_MajorArterial$AADT*County_MajorArterial$DISTANCE))/sum(County_MajorArterial$AADT*County_MajorArterial$DISTANCE))
VolumeValidation_CountyTotalVolume[2,6]=rmse(County_MajorArterial$AADT,County_MajorArterial$V1T_1)
VolumeValidation_CountyTotalVolume[2,7]=percent(VolumeValidation_CountyTotalVolume[2,6]/(sum(County_MajorArterial$AADT)/nrow(County_MajorArterial)))
VolumeValidation_CountyTotalVolume[2,8]=cor(County_MajorArterial$AADT,County_MajorArterial$V1T_1)^2
VolumeValidation_CountyTotalVolume[3,2]=nrow(County_MinorArterial)
VolumeValidation_CountyTotalVolume[3,3]=percent((sum(County_MinorArterial$V1T_1)-sum(County_MinorArterial$AADT))/sum(County_MinorArterial$AADT))
VolumeValidation_CountyTotalVolume[3,4]=percent((sum(County_MinorArterial$V1T_1*County_MinorArterial$DISTANCE)-sum(County_MinorArterial$AADT*County_MinorArterial$DISTANCE))/sum(County_MinorArterial$AADT*County_MinorArterial$DISTANCE))
VolumeValidation_CountyTotalVolume[3,6]=rmse(County_MinorArterial$AADT,County_MinorArterial$V1T_1)
VolumeValidation_CountyTotalVolume[3,7]=percent(VolumeValidation_CountyTotalVolume[3,6]/(sum(County_MinorArterial$AADT)/nrow(County_MinorArterial)))
VolumeValidation_CountyTotalVolume[3,8]=cor(County_MinorArterial$AADT,County_MinorArterial$V1T_1)^2
VolumeValidation_CountyTotalVolume[4,2]=nrow(County_Collector)
VolumeValidation_CountyTotalVolume[4,3]=percent((sum(County_Collector$V1T_1)-sum(County_Collector$AADT))/sum(County_Collector$AADT))
VolumeValidation_CountyTotalVolume[4,4]=percent((sum(County_Collector$V1T_1*County_Collector$DISTANCE)-sum(County_Collector$AADT*County_Collector$DISTANCE))/sum(County_Collector$AADT*County_Collector$DISTANCE))
VolumeValidation_CountyTotalVolume[4,6]=rmse(County_Collector$AADT,County_Collector$V1T_1)
VolumeValidation_CountyTotalVolume[4,7]=percent(VolumeValidation_CountyTotalVolume[4,6]/(sum(County_Collector$AADT)/nrow(County_Collector)))
VolumeValidation_CountyTotalVolume[4,8]=cor(County_Collector$AADT,County_Collector$V1T_1)^2
#County total VMT Modeled: 4,795,100 
sum(County_Interstates$V1T_1*County_Interstates$DISTANCE)+sum(County_MajorArterial$V1T_1*County_MajorArterial$DISTANCE/2)+sum(County_MinorArterial$V1T_1*County_MinorArterial$DISTANCE/2)+sum(County_Collector$V1T_1*County_Collector$DISTANCE/2)
#County total VMT Calculated from ADT:4,441,084
sum(County_Interstates$AADT*County_Interstates$DISTANCE/2)+sum(County_MajorArterial$AADT*County_MajorArterial$DISTANCE/2)+sum(County_MinorArterial$AADT*County_MinorArterial$DISTANCE/2)+sum(County_Collector$AADT*County_Collector$DISTANCE/2)
#County total VMT summarized in Illinois Travel Statistics 2015:4,968,860

# MPA=====
VolumeValidation_MPATotalVolume[1,2]=nrow(MPA_Interstates)
VolumeValidation_MPATotalVolume[1,3]=percent((sum(MPA_Interstates$V1T_1)*2-sum(MPA_Interstates$AADT))/sum(MPA_Interstates$AADT))
VolumeValidation_MPATotalVolume[1,4]=percent((sum(MPA_Interstates$V1T_1*MPA_Interstates$DISTANCE*2)-sum(MPA_Interstates$AADT*MPA_Interstates$DISTANCE))/sum(MPA_Interstates$AADT*MPA_Interstates$DISTANCE))
VolumeValidation_MPATotalVolume[1,8]=sum(MPA_Interstates$DISTANCE)/2
VolumeValidation_MPATotalVolume[1,9]=sum(MPA_Interstates$V1T_1*MPA_Interstates$DISTANCE)
VolumeValidation_MPATotalVolume[1,10]=percent((VolumeValidation_MPATotalVolume[1,9]-VolumeValidation_MPATotalVolume[1,7])/VolumeValidation_MPATotalVolume[1,7])
VolumeValidation_MPATotalVolume[1,11]=rmse(MPA_Interstates$AADT,MPA_Interstates$V1T_1*2)
VolumeValidation_MPATotalVolume[1,12]=percent(VolumeValidation_MPATotalVolume[1,11]/(sum(MPA_Interstates$AADT)/nrow(MPA_Interstates)))
VolumeValidation_MPATotalVolume[1,13]=cor(MPA_Interstates$AADT,MPA_Interstates$V1T_1*2)^2
VolumeValidation_MPATotalVolume[2,2]=nrow(MPA_MajorArterial)
VolumeValidation_MPATotalVolume[2,3]=percent((sum(MPA_MajorArterial$V1T_1)-sum(MPA_MajorArterial$AADT))/sum(MPA_MajorArterial$AADT))
VolumeValidation_MPATotalVolume[2,4]=percent((sum(MPA_MajorArterial$V1T_1*MPA_MajorArterial$DISTANCE)-sum(MPA_MajorArterial$AADT*MPA_MajorArterial$DISTANCE))/sum(MPA_MajorArterial$AADT*MPA_MajorArterial$DISTANCE))
VolumeValidation_MPATotalVolume[2,8]=sum(MPA_MajorArterial$DISTANCE)/2
VolumeValidation_MPATotalVolume[2,9]=sum(MPA_MajorArterial$V1T_1*MPA_MajorArterial$DISTANCE/2)
VolumeValidation_MPATotalVolume[2,10]=percent((VolumeValidation_MPATotalVolume[2,9]-VolumeValidation_MPATotalVolume[2,7])/VolumeValidation_MPATotalVolume[2,7])
VolumeValidation_MPATotalVolume[2,11]=rmse(MPA_MajorArterial$AADT,MPA_MajorArterial$V1T_1)
VolumeValidation_MPATotalVolume[2,12]=percent(VolumeValidation_MPATotalVolume[2,11]/(sum(MPA_MajorArterial$AADT)/nrow(MPA_MajorArterial)))
VolumeValidation_MPATotalVolume[2,13]=cor(MPA_MajorArterial$AADT,MPA_MajorArterial$V1T_1)^2
VolumeValidation_MPATotalVolume[3,2]=nrow(MPA_MinorArterial)
VolumeValidation_MPATotalVolume[3,3]=percent((sum(MPA_MinorArterial$V1T_1)-sum(MPA_MinorArterial$AADT))/sum(MPA_MinorArterial$AADT))
VolumeValidation_MPATotalVolume[3,4]=percent((sum(MPA_MinorArterial$V1T_1*MPA_MinorArterial$DISTANCE)-sum(MPA_MinorArterial$AADT*MPA_MinorArterial$DISTANCE))/sum(MPA_MinorArterial$AADT*MPA_MinorArterial$DISTANCE))
VolumeValidation_MPATotalVolume[3,8]=sum(MPA_MinorArterial$DISTANCE)/2
VolumeValidation_MPATotalVolume[3,9]=sum(MPA_MinorArterial$V1T_1*MPA_MinorArterial$DISTANCE/2)
VolumeValidation_MPATotalVolume[3,10]=percent((VolumeValidation_MPATotalVolume[3,9]-VolumeValidation_MPATotalVolume[3,7])/VolumeValidation_MPATotalVolume[3,7])
VolumeValidation_MPATotalVolume[3,11]=rmse(MPA_MinorArterial$AADT,MPA_MinorArterial$V1T_1)
VolumeValidation_MPATotalVolume[3,12]=percent(VolumeValidation_MPATotalVolume[3,11]/(sum(MPA_MinorArterial$AADT)/nrow(MPA_MinorArterial)))
VolumeValidation_MPATotalVolume[3,13]=cor(MPA_MinorArterial$AADT,MPA_MinorArterial$V1T_1)^2
VolumeValidation_MPATotalVolume[4,2]=nrow(MPA_Collector)
VolumeValidation_MPATotalVolume[4,3]=percent((sum(MPA_Collector$V1T_1)-sum(MPA_Collector$AADT))/sum(MPA_Collector$AADT))
VolumeValidation_MPATotalVolume[4,4]=percent((sum(MPA_Collector$V1T_1*MPA_Collector$DISTANCE)-sum(MPA_Collector$AADT*MPA_Collector$DISTANCE))/sum(MPA_Collector$AADT*MPA_Collector$DISTANCE))
VolumeValidation_MPATotalVolume[4,8]=sum(MPA_Collector$DISTANCE)/2
VolumeValidation_MPATotalVolume[4,9]=sum(MPA_Collector$V1T_1*MPA_Collector$DISTANCE/2)
VolumeValidation_MPATotalVolume[4,10]=percent((VolumeValidation_MPATotalVolume[4,9]-VolumeValidation_MPATotalVolume[4,7])/VolumeValidation_MPATotalVolume[4,7])
VolumeValidation_MPATotalVolume[4,11]=rmse(MPA_Collector$AADT,MPA_Collector$V1T_1)
VolumeValidation_MPATotalVolume[4,12]=percent(VolumeValidation_MPATotalVolume[4,11]/(sum(MPA_Collector$AADT)/nrow(MPA_Collector)))
VolumeValidation_MPATotalVolume[4,13]=cor(MPA_Collector$AADT,MPA_Collector$V1T_1)^2

#MPA total VMT Modeled: 2,716,878 
sum(MPA_Interstates$V1T_1*MPA_Interstates$DISTANCE)+sum(MPA_MajorArterial$V1T_1*MPA_MajorArterial$DISTANCE/2)+sum(MPA_MinorArterial$V1T_1*MPA_MinorArterial$DISTANCE/2)+sum(MPA_Collector$V1T_1*MPA_Collector$DISTANCE/2)
#MPA total VMT Calculated from ADT:2,759,664
sum(MPA_Interstates$AADT*MPA_Interstates$DISTANCE/2)+sum(MPA_MajorArterial$AADT*MPA_MajorArterial$DISTANCE/2)+sum(MPA_MinorArterial$AADT*MPA_MinorArterial$DISTANCE/2)+sum(MPA_Collector$AADT*MPA_Collector$DISTANCE/2)
#MPA total VMT summarized in Illinois Travel Statistics 2015: 3,203,279


# Screenline =====
ScreenlineValidation[1,2]=nrow(SL_1)
ScreenlineValidation[1,3]=sum(SL_1$V1T_1)
ScreenlineValidation[1,4]=sum(SL_1$AADT)
ScreenlineValidation[1,5]=percent((sum(SL_1$V1T_1)-sum(SL_1$AADT))/sum(SL_1$AADT))
ScreenlineValidation[2,2]=nrow(SL_2)
ScreenlineValidation[2,3]=sum(SL_2$V1T_1)
ScreenlineValidation[2,4]=sum(SL_2$AADT)
ScreenlineValidation[2,5]=percent((sum(SL_2$V1T_1)-sum(SL_2$AADT))/sum(SL_2$AADT))
ScreenlineValidation[3,2]=nrow(SL_3)
ScreenlineValidation[3,3]=sum(SL_3$V1T_1)
ScreenlineValidation[3,4]=sum(SL_3$AADT)
ScreenlineValidation[3,5]=percent((sum(SL_3$V1T_1)-sum(SL_3$AADT))/sum(SL_3$AADT))
ScreenlineValidation[4,2]=nrow(SL_4)
ScreenlineValidation[4,3]=sum(SL_4$V1T_1)
ScreenlineValidation[4,4]=sum(SL_4$AADT)
ScreenlineValidation[4,5]=percent((sum(SL_4$V1T_1)-sum(SL_4$AADT))/sum(SL_4$AADT))
ScreenlineValidation[5,2]=nrow(SL_5)
ScreenlineValidation[5,3]=sum(SL_5$V1T_1)
ScreenlineValidation[5,4]=sum(SL_5$AADT)
ScreenlineValidation[5,5]=percent((sum(SL_5$V1T_1)-sum(SL_5$AADT))/sum(SL_5$AADT))
ScreenlineValidation[6,2]=nrow(SL_6)
ScreenlineValidation[6,3]=sum(SL_6$V1T_1)
ScreenlineValidation[6,4]=sum(SL_6$AADT)
ScreenlineValidation[6,5]=percent((sum(SL_6$V1T_1)-sum(SL_6$AADT))/sum(SL_6$AADT))
ScreenlineValidation[7,2]=nrow(SL_7)
ScreenlineValidation[7,3]=sum(SL_7$V1T_1)
ScreenlineValidation[7,4]=sum(SL_7$AADT)
ScreenlineValidation[7,5]=percent((sum(SL_7$V1T_1)-sum(SL_7$AADT))/sum(SL_7$AADT))
ScreenlineValidation[8,2]=nrow(SL_8)
ScreenlineValidation[8,3]=sum(SL_8$V1T_1)
ScreenlineValidation[8,4]=sum(SL_8$AADT)
ScreenlineValidation[8,5]=percent((sum(SL_8$V1T_1)-sum(SL_8$AADT))/sum(SL_8$AADT))
ScreenlineValidation[9,2]=nrow(SL_9)
ScreenlineValidation[9,3]=sum(SL_9$V1T_1)
ScreenlineValidation[9,4]=sum(SL_9$AADT)
ScreenlineValidation[9,5]=percent((sum(SL_9$V1T_1)-sum(SL_9$AADT))/sum(SL_9$AADT))
ScreenlineValidation[10,2]=nrow(SL_10)
ScreenlineValidation[10,3]=sum(SL_10$V1T_1)
ScreenlineValidation[10,4]=sum(SL_10$AADT)
ScreenlineValidation[10,5]=percent((sum(SL_10$V1T_1)-sum(SL_10$AADT))/sum(SL_10$AADT))
ScreenlineValidation[11,2]=nrow(SL_11)
ScreenlineValidation[11,3]=sum(SL_11$V1T_1)
ScreenlineValidation[11,4]=sum(SL_11$AADT)
ScreenlineValidation[11,5]=percent((sum(SL_11$V1T_1)-sum(SL_11$AADT))/sum(SL_11$AADT))
ScreenlineValidation[12,2]=nrow(SL_12)
ScreenlineValidation[12,3]=sum(SL_12$V1T_1)
ScreenlineValidation[12,4]=sum(SL_12$AADT)
ScreenlineValidation[12,5]=percent((sum(SL_12$V1T_1)-sum(SL_12$AADT))/sum(SL_12$AADT))

# Output =====
ResultLoaction="L:/Freight Model/CUUATS Cargo Input/20 Validation/VolumeValidationResults.xlsx"
write.xlsx(VolumeValidation_CountyTotalVolume,ResultLoaction,sheetName="Champaign County Total Volume",append=TRUE)
write.xlsx(VolumeValidation_MPATotalVolume,ResultLoaction,sheetName="MPA Total Volume",append=TRUE)
write.xlsx(VolumeValidation_ValidationLinkTotalVolume,ResultLoaction,sheetName="Validation Links Total Volume",append=TRUE)
write.xlsx(ScreenlineValidation,ResultLoaction,sheetName="Screenline Total Volume",append=TRUE)
write.xlsx(VolumeValidation_CountyTruckVolume,ResultLoaction,sheetName="Champaign County Truck Volume",append=TRUE)
write.xlsx(VolumeValidation_MPATruckVolume,ResultLoaction,sheetName="MPA Truck Volume",append=TRUE)
write.xlsx(VolumeValidation_ValidationLinkTruckVolume,ResultLoaction,sheetName="Validation Links Truck Volume",append=TRUE)

